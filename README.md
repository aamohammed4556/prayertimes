# Prayer Times 

This prayer times application does not steal your data unlike some very popular options. There are no adds, no cookies, no bs load times - just asks for your location and shows you the prayer times.

## Bugs
Right now it only 100% supports US prayer times and is inconsistent outside of the US. I plan to add support worldwide soon. 

## Running on local machine

It is tricky to run on your local machine because it requires access from outside of the local network. What I did is run the application, then run ``npx localtunnel --port [whatever port]`` and access the website from there. You also must set up a free [geonames](https://www.geonames.org/login) account and put your username in the config file.  

