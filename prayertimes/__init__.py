import json
from flask import Flask
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address


def load_config():
    """Load config.json file"""
    with open("config.json", "r") as f:
        return json.load(f)


JUR_OPTIONS = {
    1: "Karachi",
    0: "Jafari",
    2: "ISNA",
    3: "MWL",
    4: "Mecca",
    5: "Egyptian",
    7: "Tehran",
    8: "Algerian",
    9: "Gulf Fixed Isha",
}

# asr and other prayers have different calculation methods
ASR_OPTIONS = {
    0: "Standard",
    1: "Hanafi",
}


CONFIG = load_config()
API_URL = CONFIG["PRAYER_API_URL"]

app = Flask(__name__)
app.secret_key = CONFIG["SECRET_KEY"]
limiter = Limiter(app, key_func=get_remote_address)

from prayertimes import routes
