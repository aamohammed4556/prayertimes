import json


class PrayerInfo:
    """
    Class that holds the info from the api request.
    This is just for easy of use in the html templates
    """

    def __init__(self, prayer_times):

        self.error = False
        # if there is a key error in the response, then there is an error with the zipcode
        try:
            self.prayer_times = json.loads(prayer_times)
            self.city = self.prayer_times["settings"]["location"]["city"]
            self.state = self.prayer_times["settings"]["location"]["state"]
            self.fajr = self.prayer_times["results"]["Fajr"]
            self.dhuhr = self.prayer_times["results"]["Dhuhr"]
            self.asr = self.prayer_times["results"]["Asr"]
            self.maghrib = self.prayer_times["results"]["Maghrib"]
            self.isha = self.prayer_times["results"]["Isha"]

            self.prayers = [
                ["Fajr", self.fajr],
                ["Dhuhr", self.dhuhr],
                ["Asr", self.asr],
                ["Maghrib", self.maghrib],
                ["Isha", self.isha],
            ]
        except KeyError:
            self.error = True
