import requests
from prayertimes import CONFIG
from prayertimes import app, limiter
from prayertimes.modals import PrayerInfo
from flask.templating import render_template
from flask import url_for, redirect, request
from prayertimes import JUR_OPTIONS, ASR_OPTIONS


def get_timezone(lat, long):
    # request timezone from timezone api.
    r = requests.get(
        f'http://api.geonames.org/timezoneJSON?lat={lat}&lng={long}&username={CONFIG["GEONAMES_USERNAME"]}'
    ).json()

    return r["timezoneId"]

def get_ip(req):
    if req.environ.get("HTTP_X_FORWARDED_FOR") is None:
        ip = req.environ["REMOTE_ADDR"]
    else:
        ip = req.environ["HTTP_X_FORWARDED_FOR"]
    return ip



@app.route("/", methods=["GET", "POST"])
@app.route("/home", methods=["GET", "POST"])
def home():
    return render_template("home.html", user_ip=get_ip(request))


@app.route("/get-location", methods=["GET"])
@limiter.limit(CONFIG["LIMIT"])
def get_location():
    query = request.args.get("q")
    # request json search from geonames api. Only get the first result
    r = requests.get(
        f'http://api.geonames.org/searchJSON?q={query}&maxRows=1&username={CONFIG["GEONAMES_USERNAME"]}'
    ).json()
    return r


@app.errorhandler(429)
def rate_limit_handler(e):
    return {"error": "Rate limit exceeded"}, 429


@app.route("/times", methods=["POST", "GET"])
def times():
    calc_meth = int(request.args.get("calc_meth"))
    asr_calc = int(request.args.get("asr_calc"))
    location = request.args.get("location")
    timezone = request.args.get("timezone")

    # make sure valid params
    if calc_meth not in JUR_OPTIONS or asr_calc not in ASR_OPTIONS:
        return redirect("/home")

    params = {
        "longitude": request.args.get("long"),
        "latitude": request.args.get("lat"),
        "method": calc_meth,
        "juristic": asr_calc,
    }

    # if the javascript did not send the timezone, sedn the user's IP to the api instead.
    if timezone == "null":
        params["user_ip"] = get_ip(request)
    else:
        timezone = get_timezone(request.args.get("lat"), request.args.get("long"))
        params["timezone"] = timezone
        print(params["timezone"])

    # Get info from islamic finder
    response = requests.request(
        "GET",
        CONFIG["PRAYER_API_URL"],
        params=params,
    )

    print(response.request.url)
    prayer_info = PrayerInfo(response.text)

    if location == "null":
        location = prayer_info.city

    return render_template(
        "home.html",
        user_ip=get_ip(request),
        prayerObj=prayer_info,
        calc_meth=JUR_OPTIONS[int(calc_meth)],
        asr_calc=ASR_OPTIONS[int(asr_calc)],
        location=location,
    )
