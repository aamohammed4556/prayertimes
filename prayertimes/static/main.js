
var showSpinner = () => {
    document.getElementById("loading").style.display = 'flex';
    document.getElementById("main-body").style.display = 'none';
}

var redirectToTimes = (lat, long, location, tz) => {
    showSpinner();

    // get asr and normal calc methods
    const asrMethod = document.getElementById("asr-options");
    const calcMethod = document.getElementById("options");

    // store for when user comes back
    localStorage.setItem("selectedMethod", calcMethod.selectedIndex);
    localStorage.setItem("selectedMethodAsr", asrMethod.selectedIndex);

    var new_url = `/times?` + new URLSearchParams({
        lat : lat,
        long : long,
        location : location,
        timezone : tz,
        calc_meth : calcMethod.value,
        asr_calc : asrMethod.value,
    });
    localStorage.setItem("lastUsedUrl", new_url);
    window.location = new_url;
}

// Code for automaticlly getting the location

function findLocation() {
    // Wait for user to click allow location
    if (window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(function(pos) {
                const { latitude, longitude } = pos.coords;
                // if the user is allowing location, there is no reason to pass timezone because we can just use their ip instead. 
                redirectToTimes(latitude, longitude, null, null);
        });
    }
}

submitForm = () => {

    var search = document.getElementById("search");
    var errorMsg = document.getElementById("search-error");

    if (search.value.length < 2) {
        errorMsg.innerText = "Please search for a longer term";
        return;
    }

    var location = fetch("/get-location?" + new URLSearchParams({
        q: search.value
    }))
    .then(response => response.json())
    .then(response => {

        if (response.error == "Rate limit exceeded") {
            errorMsg.innerText = "Too many requests. Please try again.";
            return;
        }
        if (response.totalResultsCount <= 0) {
            errorMsg.innerText = "No results found. Please narrow your search.";
            return;
        }
        console.log(response);
    // if the user submits the form, timezone is required because they may not currently be in the location they are searching for.
        redirectToTimes(response.geonames[0].lat, response.geonames[0].lng, response.geonames[0].name, "required"); 
        
    })
    .catch(error => {
        errorMsg.innerText = "Oops! Something went wrong. Please try again.";
        console.log(error);
    });
}

function main() {
    const subButton = document.getElementById("search-sub");
    const methodOptions = document.getElementById("options");
    const asrMethodOpt = document.getElementById("asr-options");

    // select the method that the user used last time
    if (localStorage.getItem("selectedMethod")) {
        methodOptions.selectedIndex = localStorage.getItem("selectedMethod");
        asrMethodOpt.selectedIndex = localStorage.getItem("selectedMethodAsr")
    }

    // user has already used the site before so bring them to their last entered location
    if (localStorage.getItem("lastUsedUrl") && window.location.pathname == "/" ) {
        showSpinner();
        window.location = localStorage.getItem("lastUsedUrl");
    }

    subButton.addEventListener("click", submitForm);
    document.getElementById("search").addEventListener("keydown", function(e) {
        if (e.key == "Enter") {
            subButton.click();
            this.value = "";
        }
    });

    document.getElementById("refresh-opts").addEventListener("click", function() {
        var url = new URL(location);
        redirectToTimes(url.searchParams.get("lat"), url.searchParams.get("long"), url.searchParams.get("location"), null);

    });

}
main()
